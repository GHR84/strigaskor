
var {name, shoes, color} = require('../main.js');


test('Búðu til <h2></h2> element með nafninu þínu.', function(){
    expect( name ).toMatch(/<h2>/);
});

test('Búðu til <span id="shoes"></span> element með uppáhalds strigaskónum þínum.', function(){
    expect( shoes ).toMatch(/<span id="shoes">/);
});

test('Búðu til <span id="color"></span> element með litnum á uppáhalds strigaskónum þínum.', function(){
    expect( color ).toMatch(/<span id="color">/);
});


// document.body.innerHTML = '<div id="main"></div>';

// var { createStory, renderStory } = require('../main.js');

// test('The main text should include a <p> element', function(){
//     expect( createStory() ).toMatch(/<p>/);
// });

// test('There should ne a <p> inside the main div', function(){
//     var getFromDom = function(){
//         renderStory();
//         return document.querySelector('#main').innerHTML;
//     }
//     expect(getFromDom()).toMatch(/<p>/);
// });

